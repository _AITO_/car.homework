class Car():
    def __init__(self , make , model , year):
        self.make = make
        self.model = model
        self.year = year
        self.odometer_reading = 0

    def description_name(self):
        """Возращаем описание автомобиля"""
        desc = str(self.year) + ' ' + self.make + ' ' + self.model
        return desc.title()

    def read_odometer(self):
        """Выводы пробег авто"""
        print("Пробег этого авто" + str(self.odometer_reading) + "км")

    def read_odometer(self,km):
        """Устанавливаем занчения на одометре"""
        if km >= self.odometer_reading:
            self.odometer_reading = km
        else:
            print("Брррр!Притормози")

    def interment_odometer(self,km):
        """Увеличиваем покозания одометра на заданную величину"""
        self.odometer_reading += km

class Battery():
    """Батарейка потомка"""

    def __init__(self, battery=100):
            self.battery = battery

    def description_battery(self):
            """Процент батареи"""
            print("У автомобиля " + str(self.battery) + "% батареи")


class ElectricCar(Car):
    """Аспекты для электро мобиля"""

    def __init__(self,make,model,year):
        """Иницеaлизация атрибутов класса родителя"""
        super().__init__(make,model,year)
        self.battery = Battery()


    def description_name(self):
        """Переопределение родительского метода"""
        desc = str(self.year) + ' ' + self.model
        return desc.title()

tesla = ElectricCar('tesla','X','2017')
tesla.battery.description_battery()






